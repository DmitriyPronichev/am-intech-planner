package ru.amintech.planner.solver;

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;
import ru.amintech.planner.domain.DaySlot;

import java.util.HashSet;
import java.util.Set;

import static org.optaplanner.core.api.score.stream.ConstraintCollectors.sum;

public class DayAssignmentConstraintProvider implements ConstraintProvider {
  private Set<Integer> testMap = new HashSet<>();

  @Override
  public Constraint[] defineConstraints(final ConstraintFactory factory) {
    return new Constraint[]{
      usageOfFreeQntShouldBeMaximized(factory),
      allocatedQntForProjectShouldBeLessThanRequestedQnt(factory),
      allocatedQntGroupedByDayShouldBeLessFreeQnt(factory),
      allocatedQuantityShouldNotBeLessThanAverageForDay(factory)
    };
  }

  // TODO: instead of this I should implement custom range that will not give numbers more than requested
  private Constraint allocatedQntForProjectShouldBeLessThanRequestedQnt(final ConstraintFactory factory) {
    return factory.from(DaySlot.class)
             .filter(v -> v.getAllocatedQuantity() > v.getProjectRequestedQuantity())
             .penalize("allocated qnt for project should be less than requested qnt",
               HardSoftScore.ofHard(1000000));
  }

  private Constraint allocatedQntGroupedByDayShouldBeLessFreeQnt(final ConstraintFactory factory) {
    return factory.from(DaySlot.class)
             .groupBy(DaySlot::getDay, sum(DaySlot::getAllocatedQuantity))
             .filter((day, allocatedQuantity) -> day.getFreeQuantity() < allocatedQuantity)
             .penalize("allocated qnt grouped by day should be less free qnt",
               HardSoftScore.ONE_HARD,
               (day, allocatedQuantity) -> allocatedQuantity - day.getFreeQuantity());
  }

  private Constraint usageOfFreeQntShouldBeMaximized(final ConstraintFactory factory) {
    return factory.from(DaySlot.class)
             .groupBy(DaySlot::getDay, sum(DaySlot::getAllocatedQuantity))
             .filter((day, allocatedQnt) -> day.getFreeQuantity() >= allocatedQnt)
             .reward("usage of free qnt should be maximized",
               HardSoftScore.ONE_SOFT,
               (day, allocatedQnt) -> day.getFreeQuantity() - (day.getFreeQuantity() - allocatedQnt));
  }

  // распределенное кол-во по проекту не должно быть меньше среднего за день
  private Constraint allocatedQuantityShouldNotBeLessThanAverageForDay(final ConstraintFactory factory) {
    return factory.from(DaySlot.class)
             .filter(daySlot -> {
               final var allocated = daySlot.getAllocatedQuantity();
               final var requested = daySlot.getProject().getRequestedQuantity();
               final var average = daySlot.getDay().getAverageQuantity();
               return requested <= average && allocated < requested;
             })
             .penalize("allocated quantity should not be less than average for day",
               HardSoftScore.ofHard(100));
  }

  private Constraint projectShouldHaveEqualAllocatedQntForPeriod(final ConstraintFactory factory) {
    return factory.from(DaySlot.class)
             .join(DaySlot.class,
               Joiners.equal(DaySlot::getProject),
               Joiners.lessThan(DaySlot::getId),
               Joiners.filtering((d1, d2) -> !d1.getAllocatedQuantity().equals(d2.getAllocatedQuantity())))
             .penalize("project should have equal allocated qnt for period", HardSoftScore.ONE_HARD);
  }

}
