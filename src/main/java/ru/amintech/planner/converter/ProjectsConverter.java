package ru.amintech.planner.converter;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import ru.amintech.planner.domain.Project;
import ru.amintech.planner.dto.ProjectRequestDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProjectsConverter {
  ProjectConverter projectConverter;

  public List<Project> convertFromRequestDTO(final List<ProjectRequestDTO> dto) {
    return dto.stream()
             .map(projectConverter::convertFromRequestDTO)
             .collect(Collectors.toList());
  }

}
