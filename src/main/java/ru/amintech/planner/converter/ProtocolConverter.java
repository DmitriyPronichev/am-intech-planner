package ru.amintech.planner.converter;

import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import ru.amintech.planner.domain.Day;
import ru.amintech.planner.domain.DaySlot;
import ru.amintech.planner.domain.Project;
import ru.amintech.planner.domain.Protocol;
import ru.amintech.planner.dto.ProjectShortResponseDTO;
import ru.amintech.planner.dto.ProtocolShortRequestDTO;
import ru.amintech.planner.dto.ProtocolShortResponseDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.minBy;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProtocolConverter {
  final DaysConverter daysConverter;
  final ProjectsConverter projectsConverter;
  long tempId = 0L;

  public static List<LocalDate> getDatesBetween(final LocalDate start, final LocalDate end) {
    final var result = start.datesUntil(end.plusDays(1)).collect(Collectors.toList());
    System.out.println("start: " + start + "; end: " + end);
    System.out.println("dates: " + result);
    return result;
  }

  public ProtocolShortResponseDTO convertToShortResponseDTO(final Protocol protocol) {
    final var projects = protocol.getProjects();
    final var daySlots = protocol.getDaySlots().stream()
                           .collect(groupingBy(DaySlot::getProject, minBy(comparingInt(DaySlot::getAllocatedQuantity))));

    final var projectsDTO = projects.stream().map(v -> ProjectShortResponseDTO.builder()
                                                         .id(v.getId())
                                                         .requestedQuantity(v.getRequestedQuantity())
                                                         .allocatedQuantity(daySlots.get(v)
                                                                              .map(DaySlot::getAllocatedQuantity)
                                                                              .orElse(0))
                                                         .build())
                              .collect(Collectors.toList());

    return ProtocolShortResponseDTO.builder()
             .projects(projectsDTO)
             .days(daysConverter.convertToShortRequest(protocol.getDays(), protocol.getDaySlots()))
             .build();
  }

  private long getNextTempId() {
    return ++tempId;
  }

  private void resetTempId() {
    tempId = 0L;
  }

  public Protocol convertFromRequestDTO(final ProtocolShortRequestDTO dto) {
    final var days = daysConverter.convertFromRequestDTO(dto.getDays(), dto.getProjects());
    final var projects = projectsConverter.convertFromRequestDTO(dto.getProjects());

    final var daysMap = days.stream().collect(Collectors.toMap(Day::getDate, v -> v));
    final var projectsMap = projects.stream().collect(Collectors.toMap(Project::getId, v -> v));

    final var minQuantity = IntStream.concat(
      days.stream().mapToInt(Day::getFreeQuantity),
      projects.stream().mapToInt(Project::getRequestedQuantity))
                              .min()
                              .orElseThrow();
    final var maxQuantity = projects.stream().mapToInt(Project::getRequestedQuantity)
                              .max()
                              .orElseThrow();

/*    final var quantities = Stream
                             .iterate(maxQuantity, v -> v > minQuantity, v -> v - 1)
                             .collect(Collectors.toList());*/
    final var quantities = Lists.reverse(
      IntStream.rangeClosed(minQuantity, maxQuantity)
        .boxed()
        .collect(Collectors.toList())
    );

    resetTempId();
    final var daySlots = dto.getProjects().stream()
                           .flatMap(project -> getDatesBetween(project.getStart(), project.getEnd())
                                                 .stream()
                                                 .map(dt -> DaySlot.builder()
                                                              .id(getNextTempId())
                                                              .day(daysMap.get(dt))
                                                              .project(projectsMap.get(project.getId()))
                                                              .allocatedQuantity(project.getRequestedQuantity())
                                                              .build()))
                           .collect(Collectors.toList());

    return Protocol.builder()
             .days(days)
             .projects(projects)
             .daySlots(daySlots)
             .quantities(quantities)
             .build();
  }

}
