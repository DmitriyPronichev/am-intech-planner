package ru.amintech.planner.converter;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import ru.amintech.planner.domain.Day;
import ru.amintech.planner.domain.DaySlot;
import ru.amintech.planner.dto.DayRequestDTO;
import ru.amintech.planner.dto.DayShortResponseDTO;
import ru.amintech.planner.dto.ProjectRequestDTO;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DaysConverter {
  DayConverter dayConverter;

  public List<Day> convertFromRequestDTO(final List<DayRequestDTO> days, final List<ProjectRequestDTO> projects) {
    return days.stream()
             .map(day -> dayConverter.convertFromRequestDTO(day, projects))
             .collect(Collectors.toList());
  }

  public List<DayShortResponseDTO> convertToShortRequest(final List<Day> days, final List<DaySlot> daySlots) {
    return days.stream()
             .map(day -> dayConverter.convertToShortResponse(day, daySlots))
             .collect(Collectors.toList());
  }

}
