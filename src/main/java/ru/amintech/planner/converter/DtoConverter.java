package ru.amintech.planner.converter;

import org.springframework.stereotype.Component;
import ru.amintech.planner.domain.Day;
import ru.amintech.planner.domain.DaySlot;
import ru.amintech.planner.domain.Identifiable;
import ru.amintech.planner.domain.Project;
import ru.amintech.planner.domain.Protocol;
import ru.amintech.planner.dto.DayDTO;
import ru.amintech.planner.dto.DaySlotRequestDTO;
import ru.amintech.planner.dto.DaySlotResponseDTO;
import ru.amintech.planner.dto.ProjectDTO;
import ru.amintech.planner.dto.ProtocolDTO;
import ru.amintech.planner.dto.ProtocolResponseDTO;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class DtoConverter {

  public static Day convertDay(final DayDTO dayDTO) {
    return Day.builder()
             .id(dayDTO.getId())
             .date(dayDTO.getDate())
             .freeQuantity(dayDTO.getFreeQuantity())
             .build();
  }

  public static DayDTO convertDay(final Day day) {
    return DayDTO.builder()
             .id(day.getId())
             .date(day.getDate())
             .freeQuantity(day.getFreeQuantity())
             .build();
  }

  public static List<DayDTO> convertDays(final List<Day> days) {
    return days.stream().map(DtoConverter::convertDay).collect(Collectors.toList());
  }

  public static Project convertProject(final ProjectDTO projectDTO) {
    return Project.builder()
             .id(projectDTO.getId())
             .requestedQuantity(projectDTO.getRequestedQuantity())
             .build();
  }

  public static ProjectDTO convertProject(final Project project) {
    return ProjectDTO.builder()
             .id(project.getId())
             .requestedQuantity(project.getRequestedQuantity())
             .build();
  }

  public static List<ProjectDTO> convertProjects(final List<Project> days) {
    return days.stream().map(DtoConverter::convertProject).collect(Collectors.toList());
  }

  public static List<Day> convert2Days(final List<DayDTO> dayDTOList) {
    return dayDTOList.stream().map(DtoConverter::convertDay).collect(Collectors.toList());
  }

  public static List<Project> convert2Projects(final List<ProjectDTO> projectDTOList) {
    return projectDTOList.stream().map(DtoConverter::convertProject).collect(Collectors.toList());
  }

  public static <T extends Identifiable> Map<Long, T> toMap(final List<T> list) {
    return list.stream().collect(Collectors.toMap(T::getId, v -> v));
  }

  public static List<DaySlot> convertDaySlots(final List<DaySlotRequestDTO> daySlotRequestDTOList,
                                              final Map<Long, Day> daysMap,
                                              final Map<Long, Project> projectsMap) {
    return daySlotRequestDTOList.stream()
             .map(v -> DaySlot.builder()
                         .id(v.getId())
                         .day(daysMap.get(v.getDayId()))
                         .project(projectsMap.get(v.getProjectId()))
                         .build())
             .collect(Collectors.toList());
  }

  public static DaySlotResponseDTO convertDaySlot(final DaySlot daySlot) {
    return DaySlotResponseDTO.builder()
             .id(daySlot.getId())
             .dayId(daySlot.getDay().getId())
             .projectId(daySlot.getProject().getId())
             .allocatedQuantity(daySlot.getAllocatedQuantity())
             .build();
  }

  public static List<DaySlotResponseDTO> convertDaySlots(final List<DaySlot> daySlots) {
    return daySlots.stream().map(DtoConverter::convertDaySlot).collect(Collectors.toList());
  }

  public static Protocol convertProtocol(final ProtocolDTO protocolDTO) {
    final var protocolBuilder = Protocol.builder();

    final var days = convert2Days(protocolDTO.getDays());
    protocolBuilder.days(days);
    final var daysMap = toMap(days);

    final var projects = convert2Projects(protocolDTO.getProjects());
    protocolBuilder.projects(projects);
    final var projectsMap = DtoConverter.toMap(projects);

    final var daySlots = convertDaySlots(protocolDTO.getDaySlots(), daysMap, projectsMap);
    protocolBuilder.daySlots(daySlots);

    final var maxQuantity = days.stream()
                              .mapToInt(Day::getFreeQuantity)
                              .max()
                              .orElseThrow(() -> new IllegalArgumentException("what the fuck?"));

    final var quantities = IntStream
                             .rangeClosed(1, maxQuantity)
                             .boxed()
                             .collect(Collectors.toList());
    //System.out.println(quantities);

    protocolBuilder.quantities(quantities);

    return protocolBuilder.build();
  }

  public static ProtocolResponseDTO toResponse(final Protocol protocol) {
    final var response = ProtocolResponseDTO.builder();
    response.days(convertDays(protocol.getDays()));
    response.projects(convertProjects(protocol.getProjects()));
    response.daySlots(convertDaySlots(protocol.getDaySlots()));
    return response.build();
  }

}
