package ru.amintech.planner.converter;

import org.springframework.stereotype.Component;
import ru.amintech.planner.domain.Project;
import ru.amintech.planner.dto.ProjectRequestDTO;

@Component
public class ProjectConverter {

  public Project convertFromRequestDTO(final ProjectRequestDTO dto) {
    return Project.builder()
             .id(dto.getId())
             .requestedQuantity(dto.getRequestedQuantity())
             .build();
  }

}
