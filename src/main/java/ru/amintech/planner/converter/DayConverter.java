package ru.amintech.planner.converter;

import org.springframework.stereotype.Component;
import ru.amintech.planner.domain.Day;
import ru.amintech.planner.domain.DaySlot;
import ru.amintech.planner.dto.DayRequestDTO;
import ru.amintech.planner.dto.DayShortResponseDTO;
import ru.amintech.planner.dto.ProjectRequestDTO;

import java.util.List;

@Component
public class DayConverter {

  public Day convertFromRequestDTO(final DayRequestDTO day, final List<ProjectRequestDTO> projects) {
    final var numOfProjectsForThisDay = projects.stream()
                                          .filter(project -> project.getStart().isBefore(day.getDate().plusDays(1)))
                                          .filter(project -> project.getEnd().isAfter(day.getDate().minusDays(1)))
                                          .count();
    final var averageQuantity = (int) (day.getFreeQuantity() / numOfProjectsForThisDay);
    return Day.builder()
             .id(day.getId())
             .date(day.getDate())
             .freeQuantity(day.getFreeQuantity())
             .averageQuantity(averageQuantity)
             .build();
  }

  public DayShortResponseDTO convertToShortResponse(final Day day, final List<DaySlot> daySlots) {
    final var allocatedQuantity = daySlots.stream()
                                    .filter(daySlot -> daySlot.getDay().equals(day))
                                    .mapToInt(DaySlot::getAllocatedQuantity)
                                    .sum();
    return DayShortResponseDTO.builder()
             .id(day.getId())
             .date(day.getDate())
             .freeQuantity(day.getFreeQuantity())
             .averageQuantity(day.getAverageQuantity())
             .allocatedQuantity(allocatedQuantity)
             .build();
  }

}
