package ru.amintech.planner.rest;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.optaplanner.core.api.solver.SolverJob;
import org.optaplanner.core.api.solver.SolverManager;
import org.springframework.stereotype.Service;
import ru.amintech.planner.converter.DtoConverter;
import ru.amintech.planner.converter.ProtocolConverter;
import ru.amintech.planner.domain.Protocol;
import ru.amintech.planner.dto.ProtocolDTO;
import ru.amintech.planner.dto.ProtocolResponseDTO;
import ru.amintech.planner.dto.ProtocolShortRequestDTO;
import ru.amintech.planner.dto.ProtocolShortResponseDTO;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProtocolService {
  SolverManager<Protocol, UUID> solverManager;
  ProtocolConverter protocolConverter;

  private Protocol solve(final Protocol problem) {
    return getFinalBestSolution(solverManager.solve(UUID.randomUUID(), problem));
  }

  private Protocol getFinalBestSolution(final SolverJob<Protocol, UUID> solverJob) {
    try {
      return solverJob.getFinalBestSolution();
    } catch (InterruptedException | ExecutionException e) {
      throw new IllegalStateException("Solving failed.", e);
    }
  }

  public ProtocolResponseDTO solve(final ProtocolDTO request) {
    final var problem = DtoConverter.convertProtocol(request);
    final var solution = solve(problem);
    return DtoConverter.toResponse(solution);
  }

  public ProtocolShortResponseDTO solveShort(final ProtocolShortRequestDTO request) {
    final var problem = protocolConverter.convertFromRequestDTO(request);
    final var solution = solve(problem);
    return protocolConverter.convertToShortResponseDTO(solution);
  }
}
