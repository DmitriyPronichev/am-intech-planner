package ru.amintech.planner.rest;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.amintech.planner.dto.ProtocolDTO;
import ru.amintech.planner.dto.ProtocolResponseDTO;
import ru.amintech.planner.dto.ProtocolShortRequestDTO;
import ru.amintech.planner.dto.ProtocolShortResponseDTO;

@RestController
@RequestMapping("/protocol")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProtocolController {
  ProtocolService protocolService;

  @PostMapping("/solve")
  public ProtocolResponseDTO solve(@RequestBody final ProtocolDTO request) {
    return protocolService.solve(request);
  }

  @PostMapping("/solveShort")
  public ProtocolShortResponseDTO solveShort(@RequestBody final ProtocolShortRequestDTO request) {
    return protocolService.solveShort(request);
  }

}
