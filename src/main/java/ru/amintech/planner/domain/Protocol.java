package ru.amintech.planner.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import java.util.List;
import java.util.stream.Collectors;

@PlanningSolution
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Protocol {

  @PlanningEntityCollectionProperty
  List<DaySlot> daySlots;

  @ProblemFactCollectionProperty
  @ValueRangeProvider(id = "dayRange")
  List<Day> days;

  @ProblemFactCollectionProperty
  List<Project> projects;

  @ProblemFactCollectionProperty
  @ValueRangeProvider(id = "quantityRange")
  List<Integer> quantities;

  @PlanningScore
  HardSoftScore score;

  @Override
  public String toString() {
    return "Protocol(\n" +
             "  daySlots: [\n    " +
             daySlots.stream().map(DaySlot::toString).collect(Collectors.joining("\n    ")) +
             "\n  ]" +
             "\n)";
  }

}
