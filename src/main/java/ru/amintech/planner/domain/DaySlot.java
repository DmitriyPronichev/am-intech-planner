package ru.amintech.planner.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DaySlot {
  long id;
  Day day;
  Project project;

  @PlanningVariable(valueRangeProviderRefs = "quantityRange")
  Integer allocatedQuantity;

  public int getProjectRequestedQuantity() {
    return project.getRequestedQuantity();
  }

  public int getDayFreeQuantity() {
    return day.getFreeQuantity();
  }

}
