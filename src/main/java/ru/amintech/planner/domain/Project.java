package ru.amintech.planner.domain;

import lombok.Builder;
import lombok.Value;

// problem fact
@Value
@Builder
public class Project implements Identifiable {
  long id;
  int requestedQuantity;
}
