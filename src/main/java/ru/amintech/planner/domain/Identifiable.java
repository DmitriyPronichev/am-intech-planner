package ru.amintech.planner.domain;

public interface Identifiable {
  long getId();
}
