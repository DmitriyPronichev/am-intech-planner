package ru.amintech.planner.domain;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class Day implements Identifiable {
  long id;
  LocalDate date;
  int freeQuantity;
  int averageQuantity;
}
