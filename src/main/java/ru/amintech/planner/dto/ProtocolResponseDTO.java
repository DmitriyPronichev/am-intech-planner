package ru.amintech.planner.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProtocolResponseDTO {
  List<DaySlotResponseDTO> daySlots;
  List<DayDTO> days;
  List<ProjectDTO> projects;
}
